import java.io.File;
import java.io.FileNotFoundException;
import java.lang.IndexOutOfBoundsException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class SameGameBoard{

	private ArrayList< ArrayList<Integer> > board;
	private int[] lastPlay = new int[2];

	public void setLastPlay(int i, int j){
		lastPlay[0] = i;
		lastPlay[1] = j;
	}

	public String getLastPlay(){
		return new String(lastPlay[0]+" "+lastPlay[1]);
	}

	public SameGameBoard(String file_name){
		readBoard(file_name);
	}

	public SameGameBoard(SameGameBoard sgb){
		board = new ArrayList< ArrayList<Integer> >();
		for(ArrayList<Integer> row : sgb.board){
			ArrayList<Integer> myrow = new ArrayList<Integer>();
			for(Integer x: row){
				myrow.add(x);
			}
			board.add(myrow);
		}
	}

	public void readBoard(String file_name){
		File f = new File(file_name);
		try{
			Scanner in = new Scanner(f);
			board = new ArrayList< ArrayList<Integer> >();
			while(in.hasNextLine()){
				String l = in.nextLine();
				Scanner line = new Scanner(l);
				ArrayList<Integer> row = new ArrayList<Integer>();
				while(line.hasNextInt()){
					int x = line.nextInt();
					row.add(x);
				}
				board.add(row);
			}
			Collections.reverse(board);
		}
		catch(FileNotFoundException e){
			System.out.println("file not found!");	
		}
	}

	public String toString(){
		String ret = new String();
		Collections.reverse(board);
		for(ArrayList<Integer> row : board){
			for(Integer x: row){
				ret += x+" ";
			}
			ret += "\n";
		}
		Collections.reverse(board);
		return ret;
	}

	private int removePositionRec(int i, int j, Integer val){
		try{
			if(board.get(i).get(j)==0 || board.get(i).get(j)!=val) return 0;
		}
		catch(IndexOutOfBoundsException e){
			return 0;
		}
		board.get(i).set(j,0);
		int total = 1;
		total += removePositionRec(i+1, j, val);
		total += removePositionRec(i, j+1, val);
		total += removePositionRec(i-1, j, val);
		total += removePositionRec(i, j-1, val);
		return total;
	}

	private SameGameBoard removePosition(int i, int j){
		SameGameBoard ret = new SameGameBoard(this);
		int total = ret.removePositionRec(i, j, ret.board.get(i).get(j));
		return (total>1)? ret:new SameGameBoard(this);
	}

	private void gravity(){
		for(int i=0; i<board.get(0).size(); ++i){
			for(int j=0; j<board.size()-1; ++j){
				if(board.get(j).get(i) == 0){
					int t;
					for(t=1; j+t<board.size() && board.get(j+t).get(i) == 0; ++t);
					if(j+t == board.size()) break;
					Integer top = board.get(j+t).get(i);
					board.get(j).set(i,top);
					board.get(j+t).set(i, 0);
				}
			}
		}
	}

	private void stickToLeft(){
		for(int i=0; i<board.get(0).size()-1; ++i){
			for(int c=1; i+c<board.get(0).size() && board.get(0).get(i) == 0; ++c){
				for(int j=0; j<board.size(); ++j){
					Integer right = board.get(j).get(i+c);
					board.get(j).set(i, right);
					board.get(j).set(i+c, 0);
				}
			}
		}
	}

	public boolean gameOver(){
		for(ArrayList<Integer> row : board){
			for(Integer x : row){
				if(x!=0) return false;
			}
		}
		return true;
	}

	public boolean equals(SameGameBoard sgb){
		return this.board.equals(sgb.board);
	}

	public SameGameBoard play(int i, int j){
		SameGameBoard t = removePosition(i,j);
		t.gravity();
		t.stickToLeft();
		t.setLastPlay(i,j);
		return t;
	}

	public ArrayList<SameGameBoard> generatePlays(){
		ArrayList<SameGameBoard> ret = new ArrayList<SameGameBoard>();
		for(int i=0; i<board.size(); ++i){
			for(int j=0; j<board.get(i).size(); ++j){
				SameGameBoard t = play(i, j);
				 if(!t.equals(this))
					ret.add(t);
			}
		}
		return ret;
	}
}