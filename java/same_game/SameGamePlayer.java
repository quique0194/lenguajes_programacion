import java.util.ArrayList;
import java.util.Stack;

public class SameGamePlayer{
    public boolean findSolution(SameGameBoard sgb, Stack<SameGameBoard> res){
        if(sgb.gameOver())
            return true;
        ArrayList<SameGameBoard> plays = sgb.generatePlays();
        for(SameGameBoard play : plays){
            if(findSolution(play, res)){
                res.push(play);
                return true;
            }
        }
        return false;
    }
}