import java.util.Stack;

public class Main{
	public static void main(String[] args) {
		SameGameBoard sgb = new SameGameBoard("input.txt");
        SameGamePlayer player = new SameGamePlayer();
        System.out.println(sgb.toString());
        Stack<SameGameBoard> plays = new Stack<SameGameBoard>();
		if(player.findSolution(sgb, plays))
            while(!plays.empty()){
                SameGameBoard t = plays.pop();
                System.out.println(t.getLastPlay() + "\n");
                System.out.println(t.toString());
            }
        else
            System.out.println("No solution found!");
	}
}