import java.io.IOException;

public class TimeBlock{
    private Time beg;
    private Time end;

    public TimeBlock(Time beg, Time end){
        this.beg = beg;
        this.end = end;
    }
    public Time getBeg(){
        return this.beg;
    }
    public Time getEnd(){
        return this.end;
    }
    public int getMinutesDuration(){
        return end.minutesFrom(beg);
    }
}