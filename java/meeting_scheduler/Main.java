import java.io.IOException;
import java.io.FileNotFoundException;

public class Main{
    public static void main(String[] args) {
        SweepLine sl = new SweepLine(2);
        PersonFactory personFactory = new PersonFactory();
        int minDuration = 45;
        Person a = new Person();
        Person b = new Person();
        try{
            a = personFactory.makePersonFromFile("a.txt");
            b = personFactory.makePersonFromFile("b.txt");
        }
        catch(IOException e){
            e.printStackTrace();
        }
        sl.addPerson(a);
        sl.addPerson(b);
        Person ret = sl.sweep(minDuration);
        System.out.println(ret.toString());
    }
}