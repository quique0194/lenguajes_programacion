import java.util.ArrayList;
import java.util.Collections;

public class SweepLine{
    int people;
    ArrayList<Event> events;

    public SweepLine(int people){
        this.people = people;
        this.events = new ArrayList<Event>();
    }
    public void addEvent(Event event){
        events.add(event);
    }
    public Person sweep(int minDuration){
        Collections.sort(events);
        Person ret = new Person();
        int actives = 0;
        Time beg = new Time();
        Time end = new Time();
        for(Event event : events){
            if(event.getType() == Event.BEG){
                actives++;
                if(actives == people){
                    beg = new Time(event.getTime());
                }
            }
            else{
                actives--;
                if(actives == people-1){
                    end = new Time(event.getTime());
                    TimeBlock block = new TimeBlock(beg, end);
                    if(block.getMinutesDuration()>=minDuration)
                        ret.addFreeTimeBlock( block );
                }
            }
        }
        return ret;
    }
    public void addPerson(Person person){
        for(TimeBlock block:person.getFreeTime()){
            addEvent( new Event(block.getBeg(), Event.BEG) );
            addEvent( new Event(block.getEnd(), Event.END) );
        }
    }
}
