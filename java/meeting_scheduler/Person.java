import java.util.ArrayList;

public class Person{ 
    private ArrayList<TimeBlock> freeTime;

    public Person(){
        freeTime = new ArrayList<TimeBlock>();
    }
    public ArrayList<TimeBlock> getFreeTime(){
        return this.freeTime;
    }
    public void addFreeTimeBlock(TimeBlock tb){
        this.freeTime.add(tb);
    }
    public String toString(){
        String ret = new String();
        String curDay = null;
        for(TimeBlock block:freeTime){
            if(curDay==null || !block.getBeg().getDay().equals(curDay)){
                curDay = block.getBeg().getDay();
                ret += "\n" + curDay;
            }
            ret += " " + block.getBeg().getTimeOfDay() + "-" + block.getEnd().getTimeOfDay();
        }
        return ret;
    }
    public boolean isValid(){
        SweepLine sl = new SweepLine(2);
        sl.addPerson(this);
        Person intersections = sl.sweep(0);
        return intersections.getFreeTime().isEmpty();
    }
}