import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class PersonFactory{
    public Person makePersonFromFile(String filename) throws IOException{
        Person person = new Person();
        File f = new File(filename);
        Scanner in = new Scanner(f);
        while(in.hasNextLine()){
            String line = in.nextLine();
            if(line.isEmpty()) continue;
            if(!line.matches("[a-z]{3}([ ]+\\d{2}:\\d{2}-\\d{2}:\\d{2})+[ ]*")) 
                throw new IOException("Invalid input");
            String[] tokens = line.split("[ ]+");
            String day = tokens[0];
            for(int i=1; i<tokens.length; ++i){
                String[] times = tokens[i].split("-");
                Time beg = new Time(day, times[0]);
                Time end = new Time(day, times[1]);
                person.addFreeTimeBlock( new TimeBlock(beg, end) );
            }
        }
        if(person.isValid()) return person;
        else throw new IOException("Free time intersects");
    }
}