import java.io.IOException;
import java.lang.Comparable;

public class Time implements Comparable<Time>{
    private final static int MINUTES_PER_HOUR = 60;
    private final static int MINUTES_PER_DAY = MINUTES_PER_HOUR*24;
    private final static String[] weekdays = {"mon", "tue", "wed", "thu", "fri", "sat", "sun"};
    
    int time;

    public Time(){
        this.time = 0;
    }
    public Time(Time t){
        this.time = t.time;
    }
    public Time(String day, String time) throws IOException{
        int day_index = getIndexOfDay(day);
        this.time = day_index*MINUTES_PER_DAY;
        String[] tokens = time.split(":");
        Integer hh = new Integer(tokens[0]);
        Integer mm = new Integer(tokens[1]);
        if(day_index == -1 || hh>=24 || mm>=60)
            throw new IOException("Invalid time format");
        this.time += 60*hh + mm;
    }
    private int getIndexOfDay(String day){
        for(int i=0; i<weekdays.length; ++i ){
            if(weekdays[i].equals(day))
                return i;
        }
        return -1;
    }
    public int compareTo(Time t){
        return this.time - t.time;
    }
    public String getDay(){
        return weekdays[time/MINUTES_PER_DAY];
    }
    public String getTimeOfDay(){
        String ret = new String();
        int hour =  (time%MINUTES_PER_DAY)/MINUTES_PER_HOUR;
        int minute = time%MINUTES_PER_DAY%MINUTES_PER_HOUR;
        ret +=  ((hour<10)? "0"+hour:hour) + ":" + ((minute<10)? "0"+minute:minute);
        return ret;
    }
    public int minutesFrom(Time t){
        return this.time - t.time;
    }
};