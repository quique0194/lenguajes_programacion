import java.lang.Comparable;

public class Event implements Comparable<Event>{
    public final static int END = -1;
    public final static int BEG = 1; 
    private Time time;
    private int type;

    public Event(Time time, int type){
        this.time = time;
        this.type = type;
    }

    public int compareTo(Event e){
        int temp = this.time.compareTo(e.time);
        return (temp == 0)? this.type - e.type : temp;
    }

    public int getType(){
        return this.type;
    }
    public Time getTime(){
        return this.time;
    }
}