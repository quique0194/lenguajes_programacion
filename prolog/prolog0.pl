causes(measles, spots).
causes(mumps, swollen_glands).

known(spots).

diagnosis(Symptom,Disease) :-
	causes(Disease,Symptom),
	known(Symptom).

mipred(X) :-
   display(X),
   nl.

ejemplo :-
    X = mipred(5),
    call(X).
