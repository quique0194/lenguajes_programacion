% Cognitive systems engineering
% Example of a simple diagnosis program written in Prolog with example
% of ontological inference
%
% John Fox ............ version 1, November 2009

% a knowledge base consisting of facts about a simple medical "domain"

causes(measles, rash).                % diseases cause symptoms
causes(meningitis,skin_rash).
causes(meningitis,fever).
causes(mumps, swollen_glands).

ontologyAKO(skin_rash,rash).	      % "a kind of" (AKO) declarations 
ontologyAKO(red_spots,skin_rash).

% a simple "patient record" consisting of facts about a patient.

known(red_spots).
known(fever).
known(swollen_glands).

% part of the "known" ontology, providing inference over classes

known(SymptomClass) :-
	ontologyAKO(Symptom,SymptomClass),
	known(Symptom).

% a very simple diagnosis procedure which determines which symptoms are
% known and the conditions that could cause the symptom.

diagnosis(Condition) :-
	known(Symptom),
	causes(Condition,Symptom).

% alternate knowledge base, showing how the same diagnosis procedure
% could be applied in the domain of fault diagnosis

known(wet_plugs).

causes(car_wont_start,electrical_fault).
causes(car_wont_start,no_petrol).

ontologyAKO(wet_plugs, electrical_fault).

% a procedural recursive program (not pure!) in prolog

printit([First|Rest]) :-
	write(First),nl,
      	printit(Rest).

