
board([ [2,1,3,1],
        [2,2,2,3],
        [2,3,3,1]]).
winner([[0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0]]).
list([2,1,5,3,12,5]).

%PRINT_FUNCTIONS

print_list([]).
print_list([H|T]):-print(H), print(' '), print_list(T).

print_matrix([]).
print_matrix([H|T]):-print_list(H), nl, print_matrix(T).

%AUXILIAR_FUNCTIONS

get_list_pos_aux([H|_],Pos,Pos,H):-!.
get_list_pos_aux([_|T],Pos,I,Res):-NextI is I+1,get_list_pos_aux(T,Pos,NextI,Res).

set_list_pos_aux([_|T],I,I,Val,[Val|T]):-!.
set_list_pos_aux([H|T],I,Cont,Val,[H|Temp]):-NewCont is Cont+1, set_list_pos_aux(T,I,NewCont,Val,Temp).

%LIST_FUNCTIONS

get_list_pos(List,Pos,Res):-get_list_pos_aux(List,Pos,0,Res).

set_list_pos(List,I,Val,ResList):-set_list_pos_aux(List,I,0,Val,ResList).

%MATRIX_FUNCTIONS

get_matrix_pos(Matrix,I,J,Res):-get_list_pos(Matrix,I,Row),get_list_pos(Row,J,Res),!.
get_matrix_pos(_,_,_,-1).

set_matrix_pos(Matrix,I,J,Val,ResMat):-get_list_pos(Matrix,I,Row), set_list_pos(Row,J,Val,NewRow), set_list_pos(Matrix,I,NewRow,ResMat).

%SAME_GAME

flood_fill_aux(Mat,_,_,0,0,Mat):-!.
flood_fill_aux(Mat,I,J,Color,0,Mat):-get_matrix_pos(Mat,I,J,C),C\=Color,!.
flood_fill_aux(Mat,I,J,Color,Cont,ResMat):-get_matrix_pos(Mat,I,J,C),C==Color,
                    set_matrix_pos(Mat,I,J,0,Mat2),
                    I1 is I-1, J1 is J, flood_fill_aux(Mat2,I1,J1,Color,C1,Mat3),
                    I2 is I, J2 is J+1, flood_fill_aux(Mat3,I2,J2,Color,C2,Mat4),
                    I3 is I+1, J3 is J, flood_fill_aux(Mat4,I3,J3,Color,C3,Mat5),
                    I4 is I, J4 is J-1, flood_fill_aux(Mat5,I4,J4,Color,C4,ResMat),
                    Cont is 1+C1+C2+C3+C4.

% Given a pos (I,J) converts to 0 every equal neighbour number
flood_fill(Mat,I,J,Cont,ResMat):-get_matrix_pos(Mat,I,J,Color),flood_fill_aux(Mat,I,J,Color,Cont,ResMat).

%%%%%%%

make_til_fall_down(Mat,I,J,Mat):-NextI is I+1, get_matrix_pos(Mat,NextI,J,Val), Val\=0, !.
make_til_fall_down(Mat,I,J,ResMat):-get_matrix_pos(Mat,I,J,Val),
                    NextI is I+1,
                    set_matrix_pos(Mat,NextI,J,Val,Mat1),
                    set_matrix_pos(Mat1,I,J,0,Mat2),
                    make_til_fall_down(Mat2,NextI,J,ResMat),!.


reacomodate_col_aux(Mat,I,_,Mat):-I<0,!.
reacomodate_col_aux(Mat,I,J,ResMat):-make_til_fall_down(Mat,I,J,Mat1), 
                    PrevI is I-1, reacomodate_col_aux(Mat1,PrevI,J,ResMat),!.
reacomodate_col(Mat,J,ResMat):-length(Mat, Size), 
                    I is Size-1, reacomodate_col_aux(Mat,I,J,ResMat),!.

reacomodate_all_cols_aux(Mat,J,Mat):-J<0,!.
reacomodate_all_cols_aux(Mat,J,ResMat):-reacomodate_col(Mat,J,Mat1),
                    PrevJ is J-1, reacomodate_all_cols_aux(Mat1,PrevJ,ResMat),!.

% Makes all columns to fall down
reacomodate_all_cols([FirstRow|Rows],ResMat):-length(FirstRow,Cols),
                    J is Cols-1, reacomodate_all_cols_aux([FirstRow|Rows],J,ResMat).

%%%%%%%

move_til_to_left(Mat,I,J,ResMat):-get_matrix_pos(Mat,I,J,Val),
                    PrevJ is J-1, set_matrix_pos(Mat,I,PrevJ,Val,Mat1),
                    set_matrix_pos(Mat1,I,J,0,ResMat).

move_column_to_left_aux(Mat,I,_,Mat):-I<0,!.
move_column_to_left_aux(Mat,I,J,ResMat):-move_til_to_left(Mat,I,J,Mat1),
                    PrevI is I-1, move_column_to_left_aux(Mat1,PrevI,J,ResMat),!.
move_column_to_left(Mat,J,ResMat):-length(Mat,Size),
                    I is Size-1, move_column_to_left_aux(Mat,I,J,ResMat).


% Moves columnt to left until it finds another column
move_column_to_left_rec(Mat,J,ResMat):-length(Mat,Size), 
                    LastI is Size-1, PrevJ is J-1,
                    get_matrix_pos(Mat,LastI,PrevJ,0),
                    move_column_to_left(Mat,J,Mat1),
                    move_column_to_left_rec(Mat1,PrevJ,ResMat),!.
move_column_to_left_rec(Mat,_,Mat).


move_board_to_left_aux([FirstRow|Rows],J,[FirstRow|Rows]):-length(FirstRow,J),!.
move_board_to_left_aux(Mat,J,ResMat):-move_column_to_left_rec(Mat,J,Mat1),
                    NextJ is J+1, move_board_to_left_aux(Mat1,NextJ,ResMat),!.
% It doesn't leave spaces to the left or between columns
move_board_to_left(Mat,ResMat):-move_board_to_left_aux(Mat,1,ResMat).

%%%%%%%

% Acomodates board after a play is done
acomodate_board(Mat,ResMat):-reacomodate_all_cols(Mat,Mat1),
                    move_board_to_left(Mat1,ResMat).

%%%%%%%

play(Mat,I,J,ResMat):-flood_fill(Mat,I,J,Cont,Mat1),Cont>1,
                    acomodate_board(Mat1,ResMat).

%%%%%%%

row_of_zeros([]).
row_of_zeros([0|Tail]):-row_of_zeros(Tail).

% Tells if a game is over
win([]).
win([FirstRow|Rows]):-row_of_zeros(FirstRow),win(Rows).


solver_aux(Mat,I,J):-
                    play(Mat,I,J,Mat1), solver(Mat1), 
                    print(I), print(' '), print(J), nl, nl,
                    print_matrix(Mat), nl, !.
solver_aux([FirstRow|Rows],I,J):-
                    length(FirstRow,J), NextI is I+1, !,
                    solver_aux([FirstRow|Rows],NextI, 0),!.
solver_aux(Mat,I,J):-
                    length(Mat,Size), I<Size,
                    NextJ is J+1, solver_aux(Mat,I,NextJ),!.

% Solves a samegame 
solver(Mat):-win(Mat), print_matrix(Mat), nl, !.
solver(Mat):-solver_aux(Mat,0,0).