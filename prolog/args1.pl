% Modify the following prolog program so that arguments are weighted 
% to represent the strength of individual arguments. Use the weight 
% to determine whether an attacking argument defeats the attacked argument. 

% inference engine - this rule uses general patterns to construct arguments
% for a claim, but the argument will only be accepted if it is not defeated.

acceptable(Claim,Argument) :-
     argument_for(Claim,Argument),
	call(Argument),
     \+ defeated(Claim,Argument).

% this rule checks whether an argument can be defeated based on general lines of
% attack and specific knowledge about the application domain

defeated(Claim,Argument) :-
     argument_against(Argument,Attack),
	call(Attack).

% ---------------------------------------------------------------------------

% general argument "schemas" for arguing about use of drugs - these specify 
% generallines of argument (templates) for or against particular claims. Add
% other templates to introduce other lines of argument for and against use of
% drugs

argument_for(give_drug(Condition,Drug),use_for(Drug,Condition)).

argument_against(use_for(Drug,Condition),cost(Drug,expensive)).
argument_against(use_for(Drug,Condition),contraindication(Drug,unsafe)).

% ---------------------------------------------------------------------------

% specific facts about drugs and hypertension - remove the comment marks (%)
% from facts below to have them used in the argumentation, or add additional
% facts to cover other drugs, contraindications etc.

use_for(statins,hypertension).
use_for(aspirin,hypertension).
use_for(beta_blockers,hypertension).
use_for(ace_inhibitors,hypertension).

cost(aspirin,cheap).
cost(statins,cheap).
% cost(beta_blockers,expensive).

contraindication(aspirin,unsafe).
% contraindication(ace_inhibitors,unsafe).