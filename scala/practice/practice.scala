
object practice{
    
    def last(l:List[Int]):Int = if(l.size == 1) l.head else last(l.tail)
    
    def last_but_one(l:List[Int]):Int = if(l.size == 2) l.head else last_but_one(l.tail)   

    def nth(n:Int, l:List[Int]):Int = l(n)

    def length(l:List[Int]):Int = if(l.isEmpty) 0 else 1 + length(l.tail)    

    def reverse(l:List[Int]):List[Int] = if(l.isEmpty) l else l.last :: reverse(l.init)

    def reverse2(l:List[Int]):List[Int] = {
        var ret = List[Int]()
        for( i <- l ) ret = i :: ret;
        ret
    }

    def palindrome(l:List[Int]):Boolean = 
        if(l.size < 2) true 
        else if( l.head == l.last ) palindrome(l.tail.init) 
        else false

    def palindrome2(l:List[Int]):Boolean = l == reverse(l)

}
