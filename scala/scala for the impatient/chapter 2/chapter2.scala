object exercise1{
    def signum(n: Int) = if(n > 0) 1 else if(n < 0) -1 else 0

    def main(args: Array[String])={
        while(true){
            val n = readInt()
            println(signum(n))
        }
    }
}



object exercise5{
    def countdown(n: Int){ for( i <- 0 to n; j = n-i ) println(j)}
    def main(args: Array[String]) = countdown(10)
}


object exercise7{
    def main(args: Array[String]){
        var r = 1
        var s = readLine("input: ")
        s.foreach(r*=_)
        println(r)
    }
}


object exercise8{
    def product(s: String) = {
        var r = 1
        for( c <- "Hello" ) r *= c
        r
    }
    def main(args: Array[String]){
        println(product("Hello"))
    }
}



object exercise9{
    def product(s: String): Int = if(s.size == 0) 1 else s.head * product(s.tail)
    def main(args: Array[String]){
        println(product("Hello"))
    }
}



object exercise10{
    def pow(x: Int, n: Int): Double = {
        if( n == 0 ) 1 
        else if( n < 0 ) 1/pow(x,-n); 
        else if( n%2 == 0 ){
            val y = pow(x,n/2)
            y*y
        }
        else{
            x * pow(x,n-1)
        }
    }
    def main(args: Array[String]){
        println(pow(2,5))
    }
}