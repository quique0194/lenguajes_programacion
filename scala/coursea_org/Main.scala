
object pascal{
    def pascal_aux(a:Int, b:Int): Int = {
        if(a==b || b==0) 1
        else pascal_aux(a-1, b-1) + pascal_aux(a-1, b)
    }
    def pascal(c:Int, r:Int) = pascal_aux(r,c)
    def main(args: Array[String]){
        while(true){
            print("Ingrese c y r: ")
            val c = readInt()
            val r = readInt()
            println(pascal(c,r))
        }
    }
}


object parentheses{
    def balance_aux(s: List[Char]): Int = {
        if( s.size == 0 ) 0 
        else{
            val r = balance_aux(s.tail)
            if(r > 0) 1
            else r + {
                if( s.head == '(' ) 1
                else if( s.head == ')' ) -1
                else 0
            }
        }
    }
    def balance(chars: List[Char]): Boolean = if(balance_aux(chars) == 0) true else false
    def main(args: Array[String]){
        while(true){
            var s = readLine("Ingrese cadena: ")
            println(balance(s.toList))
        }
    }
}


object counting{
    def countChange(money:Int, coins:List[Int], min:Int = 0): Int = {
        if(money == 0) 1
        else{
            var r = 0
            for( i <- coins if i <= money && i >= min ) r += countChange(money-i, coins, i)
            r
        }
    }
    def main(args: Array[String]){
        print("Ingrese dinero: ")
        val money = readInt()
        print("Ingrese monedas: ")
        var coin = readInt()
        var coins : List[Int] = List()
        while( coin != 0 ){
            coins = coin :: coins
            coin = readInt()
        }
        println(countChange(money, coins))
    }
}