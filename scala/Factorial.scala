
object Factorial{
    def factorial(n: Int):Int={
        if(n<2) n else n*factorial(n-1);
    }
    def main(args: Array[String]){
        println(factorial(5));
    }
}
