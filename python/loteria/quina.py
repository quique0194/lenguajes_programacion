from loteria import AbstractLoteria

class Quina(AbstractLoteria):
    nombre = 'Quina'
    precios = dict([(5,0.75),(6,3),(7,7.5)])
    porc_reparticion = 0.308134
    porc_ganancia = dict([(3,0.25),(4,0.25),(5,0.35)])
    def __init__(self, directorio):
        super(Quina, self).__init__(directorio)
        self.nro_ganadores = dict([(3,0),(4,0),(5,0)])
        self.ganancia_ganadores = dict([(3,0),(4,0),(5,0)])

    def parsear_apuesta(self, apuesta):
        return map(lambda x: int(x), apuesta.replace('\n','').split(' '))

    def parsear_resultado(self, resultado):
        return self.parsear_apuesta(resultado)

    def puntuar_apuesta(self, apuesta):
        return sum([ 1 for n in apuesta if n in self.resultado])

    def contar_ganador(self, apuesta, puntos):
        if puntos in self.nro_ganadores:
            self.nro_ganadores[puntos] += 1

    def precio_apuesta(self, apuesta):
        return Quina.precios[len(apuesta)]

    def repartir_ganancia(self):
        for i in Quina.porc_ganancia:
            if self.nro_ganadores[i] == 0:
                self.ganancia_ganadores[i] = 0
            else:
                self.ganancia_ganadores[i] = (Quina.porc_ganancia[i] * Quina.porc_reparticion * self.ganancia) / self.nro_ganadores[i]

    def imprimir_resultado(self):
        print self.ganancia
        print "%.2f"%(self.ganancia * self.porc_reparticion)
        print "Quina", self.nro_ganadores[5], "%.2f"%self.ganancia_ganadores[5]
        print "Quadra", self.nro_ganadores[4],  "%.2f"%self.ganancia_ganadores[4]
        print "Terno", self.nro_ganadores[3],  "%.2f"%self.ganancia_ganadores[3]



l = Quina('quina')
l.sortear()
l.imprimir_resultado()



