# -*- coding: utf-8 -*-
from loteria import AbstractLoteria

class Apuesta:
    def __init__(self, n, partidos):
        self.n = n
        self.partidos = partidos

    def __getitem__(self, index):
        return self.partidos[index]

    def __len__(self):
        return len(self.partidos)

class Lotogol(AbstractLoteria):
    nombre = 'Lotogol'
    precios = dict([(1,0.5),(2,1),(4,2)])
    porc_reparticion = 0.26819923372
    porc_ganancia = dict([(3,0.3),(4,0.3),(5,0.4)])

    def __init__(self, directorio):
        super(Lotogol, self).__init__(directorio)
        self.nro_ganadores = dict([(3,0),(4,0),(5,0)])
        self.ganancia_ganadores = dict([(3,0),(4,0),(5,0)])

    def parsear_apuesta(self, apuesta):
        return Apuesta(int(apuesta[0]), self.parsear_resultado(apuesta[2:]))

    def parsear_resultado(self, resultado):
        a = resultado.replace('\n', '').replace('+','4').split(' ')
        a = map(lambda x: int(x), a)
        ret = []
        for i,j in zip(xrange(0,len(a),2), xrange(1,len(a),2)):
            ret.append((a[i],a[j]))
        return ret

    def puntuar_apuesta(self, apuesta):
        return sum([ 1 for i in xrange(len(apuesta)) if apuesta[i] == self.resultado[i] ])

    def contar_ganador(self, apuesta, puntos):
        if puntos in self.nro_ganadores:
            self.nro_ganadores[puntos] += apuesta.n

    def precio_apuesta(self, apuesta):
        return Lotogol.precios[apuesta.n]

    def repartir_ganancia(self):
        for i in Lotogol.porc_ganancia:
            if self.nro_ganadores == 0:
                self.ganancia_ganadores[i] = 0
            else:
                self.ganancia_ganadores[i] = (Lotogol.porc_ganancia[i] *
                    Lotogol.porc_reparticion * self.ganancia) / self.nro_ganadores[i]

    def imprimir_resultado(self):
        print self.ganancia
        print "%.2f"%(self.ganancia * self.porc_reparticion)
        print "1º (5 acertos)", self.nro_ganadores[5], "%.2f"%self.ganancia_ganadores[5]
        print "2º (4 acertos)", self.nro_ganadores[4],  "%.2f"%self.ganancia_ganadores[4]
        print "3º (3 acertos)", self.nro_ganadores[3],  "%.2f"%self.ganancia_ganadores[3]



l = Lotogol('lotogol')
l.sortear()
l.imprimir_resultado()

