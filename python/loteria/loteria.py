
class AbstractLoteria(object):
    nombre = 'Loteria'

    def __init__(self, directorio):
        self.ganancia = 0
        self.arc_apuestas = directorio+'/apuestas'
        self.arc_resultado = directorio+'/resultado'

    def parsear_apuesta(self, apuesta):
        raise NotImplementedError()

    def parsear_resultado(self, resultado):
        raise NotImplementedError()

    def leer_apuestas(self):
        with open(self.arc_apuestas,'r') as f:
            for apuesta in f:
                if apuesta.replace('\n','').replace(' ',''):
                    yield self.parsear_apuesta(apuesta)

    def leer_resultado(self):
        with open(self.arc_resultado, 'r') as f:
            self.resultado = self.parsear_resultado(f.read())

    def puntuar_apuesta(self, apuesta):
        raise NotImplementedError()

    def contar_ganador(self, puntos):
        raise NotImplementedError()

    def precio_apuesta(self, apuesta):
        raise NotImplementedError()

    def calcular_numero_ganadores(self):
        for apuesta in self.leer_apuestas():
            puntos = self.puntuar_apuesta(apuesta)
            self.contar_ganador(apuesta, puntos)

    def calcular_ganancia(self):
        for apuesta in self.leer_apuestas():
            self.ganancia += self.precio_apuesta(apuesta)
    
    def repartir_ganancia(self):
        raise NotImplementedError()

    def sortear(self):
        self.leer_apuestas()
        self.leer_resultado()
        self.calcular_numero_ganadores()
        self.calcular_ganancia()
        self.repartir_ganancia()

    def imprimir_resultado(self):
        raise NotImplementedError()

